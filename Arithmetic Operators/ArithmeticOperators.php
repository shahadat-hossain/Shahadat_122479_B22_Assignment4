<?php
##Arithmetic Operators

$a = 35;
$b = 10;


##Negation
echo -$a;
echo "<hr>";


##Addition
$c = $a + $b;
echo $c;
echo "<hr>";


##Subtraction
$d = $a - $b;
echo $d;
echo "<hr>";


##Multiplication
$e = $a * $b;
echo $e;
echo "<hr>";


##Division
$f = $a / $b;
echo $f;
echo "<hr>";


##Modulus
$g = $a % $b;
echo $g;
echo "<hr>";


##Exponentiation
$h = $a ** $b;
echo $h;


?>