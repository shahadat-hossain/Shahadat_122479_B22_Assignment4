<?php

//Greater Than Comparison

$a = 80;
$b = 70;

if( $a > $b ) {
    echo "True";
} else {
    echo "False";
}

echo "<hr>";

$c = 50;
$d = 80;

if( $c > $d ) {
    echo "True";
} else {
    echo "False";
}

?>