<?php

//Less Than Comparison

$a = 20;
$b = 10;

if( $a < $b ) {
    echo "True";
} else {
    echo "False";
}

echo "<hr>";

$c = 20;
$d = 50;

if( $c < $d ) {
    echo "True";
} else {
    echo "False";
}

?>